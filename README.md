# Projeto final DevOps

Este projeto final tem como objetivo apresentar os principais conceitos aprendidos ao longo do Programa de Bolsas DevOps promovido pela [Compass UOL](https://compass.uol/).

O projeto consiste em:
- Criar uma máquina virtual com o Oracle Linux;
- Instalar o Docker e o Kubernetes;
- Instalar o Jenkins no Kubernetes;
- Criar os manifests no Kubernetes para um site Wordpress e banco MySQL 5.6;
- Criar um pipeline no jenkins, onde ao disparar esse job, fará a criação de um site Wordpress dentro do Kubernetes;
- Validar o funcionamento do site Wordpress através da URI *projetofinal.compass.uol*;

Os principais conceitos abordados neste projeto são:
- Linux
- Redes
- Docker
- Kubernetes
- Cloud
- Infra como Código (IaC)
- CI/CD

Toda a história do projeto pode ser acompanhada através dos [Issue Boards do GitLab](https://gitlab.com/JadsonBraz/final-devops/-/boards).

O arquivo `README.md` é o primeiro arquivo a ser lido quando o projeto é acessado. Toda a documentação está na pasta `docs`.

## Instalação

Os tutoriais de instalação e configuração do projeto estão na [pasta docs](docs). Abaixo será listada a sequência que deve ser seguida para realizar a instalação do projeto.

### Instalar e configurar a VM

Siga uma das duas instruções abaixo:

- [Instalar e configurar máquina virtual local](docs/install_oracle.md)

- [Instalação em máquinas EC2 da AWS utilizando Terraform](docs/config_aws_ec2.md)

### Configurar o Ansible

- [Configurar o Ansible](docs/config_ansible.md)


## Instalação do Docker

- [Instalação do Docker](docs/install_docker.md)
	

### Instalação do Kubernetes

Siga uma das duas instruções abaixo:

- [Instalação do Kubernetes (minikube)](docs/install_k8s_minikube.md)

- [Instalação do Kubernetes (kubeadm)](docs/install_k8s_kubeadm.md)

### Instalação do Jenkins

- [Instalação do Jenkins no Kubernetes](docs/install_jenkins.md)

## Instalação do MySQL

- [Instalação do MySQL no Kubernetes](docs/install_mysql.md)

## Instalação do Wordpress

A instalação do Wordpress é feita através de um pipeline no Jenkins.

- [Criar um pipeline no Jenkins](docs/create_pipeline.md)


## Autores

* **Jadson Braz** - [JadsonBraz](https://gitlab.com/JadsonBraz)
* **Heloisa Gomes** - [hhsgomes](https://gitlab.com/hhsgomes)


## Licença

Este projeto está licenciado sob a Licença MIT.
