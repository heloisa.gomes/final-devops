# Instalalação do kubernetes utilizando o Kubeadm

Esse documento apresenta a instalação do kubernetes utilizando o **Kubeadm**.

## Instalando o Kubeadm pelo Ansible Playbook

Caso tenha o [Ansible instalado e devidamente configurado](config_ansible.md), o Kubeadm pode ser instalado através do comando, dentro da pasta `src/ansible`:

		ansible-playbook -i hosts install_kubernetes.yml

Após a instalação do kubernetes, você pode prosseguir para a etapa de [configuração dos nodes](#configura%C3%A7%C3%A3o-do-kubeadm).


## Instalando o Kubeadm manualmente

Caso queira instalar manualmente, siga os passos abaixo:

copie o script `install_k8s_kubeadm.sh` localizado em `src/ansible/roles/kubernetes/files/` para todas as suas máquinas virtuais e execute-o:

		sh install_kubernetes.sh

Após a instalação do kubernetes, você pode prosseguir para a etapa de [configuração dos nodes](#configura%C3%A7%C3%A3o-do-kubeadm).


## O que o script faz?

O script `install_k8s_kubeadm.sh` faz uma série de modificações no sistema preparando-o para a instalação do kubernetes. Depois de preparar o sistema, as ferramentas **kubeadm** e **kubectl** serão instaladas, junto com o serviço **kubelet**.

Então o script executará as seguintes tarefas:
- Desabilitar o SELinux, para permitir que os containers acessem o sistema de arquivos do sistema, permitindo que os pods e os serviços de rede funcionem corretamente.

- Desabilitar o Firewall.

- Adicionar o repositório do kubernetes.

- Mudar o cgroup do docker.

- Instalar o kubernetes.

- Habilitar o kubelet.

Após tudo isso o kubernetes vai estar pronto para ser configurado.

## Configuração do Kubeadm

Após a instalação do kubernetes, você deve configurar os nodes (master e workers).

### Node master

Para configurar a máquina master, execute os comandos abaixo:		

		sudo kubeadm init --pod-network-cidr=10.244.0.0/16

No final do processo, você deve obter o token do master, que será utilizado pelos workers para se conectar ao master.

Ainda no master execute os comandos abaixo para configurar a ferramenta **kubectl**:

		mkdir -p $HOME/.kube
		sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
		sudo chown $(id -u):$(id -g) $HOME/.kube/config

Ao executar o comando `kubectl get nodes`, você verá que o master já foi criado.

Agora configure os worker nodes.

### Node worker

Para configurar os workers, execute os comandos abaixo, trocando o `token` e o `ip` para os que estão configurados no master:

		sudo kubeadm join --token <token> <ip>:6443

O comando vai ficar semelhante ao exemplo abaixo:

		sudo kubeadm join 172.31.78.7:6443 --token zji0zo.25hdm3way6gijsez --discovery-token-ca-cert-hash sha256:829ca70e0d0477b516e2e04ac3cea075ec59f3d833d448a53153b8e12eb57110


Ao executar o comando `kubectl get nodes`, você verá o master e os workers que já foram criados.

Agora configure a rede do kubernetes.

### Flannel Network

O kubernetes precisa de um plugin rede (CNI) para funcionar corretamente. Neste projeto utilizamos o [Flannel](https://coreos.com/flannel/).

Para aplicar o Flannel, execute o comandos abaixo:

		kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml

Depois de configurar a rede, iremos adicionar o NGINX Ingress Controller.


### NGINX Ingress Controller

O NGINX Ingress Controller fará o balanceamento de carga para as rotas das aplicações presentes no cluster.

Para instalar o NGINX, basta copiar o script `install_ingress.sh` localizado em `src/ansible/roles/kubernetes/files/` na máquina **master** e executar o comando:

		sh install_ingress.sh

Acesse o IP da máquina no navegador e veja se a página padrão do NGINX apareceu.


## Próximos passos

Após instalar e configurar kubernetes, você pode prosseguir para a etapa de [instalação do Jenkins](./install_jenkins.md).


## Referências

https://techviewleo.com/deploy-kubernetes-cluster-on-oracle-linux-with-kubeadm/

https://gist.github.com/rkaramandi/44c7cea91501e735ea99e356e9ae7883

https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-manifests/
